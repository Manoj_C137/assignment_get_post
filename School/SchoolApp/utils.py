from .exceptions import NotAlphabetic, NotNumeric, Limit

def _validate_name(name):
	if "".join(name.split()).isalpha():
		name_len = len(name)
		if (name_len < 2) or (name_len > 60):
			raise Limit("Name should have 2 to 50 characters")
	else:
		raise NotAlphabetic("Name should be alphabetic")

def _validate_number(number):
	try:
		int(number)
	except ValueError:
		raise NotNumeric("Number should contains Digits only.")
