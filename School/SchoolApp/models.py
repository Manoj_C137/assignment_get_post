from django.db import models

class Department(models.Model):
	department_id = models.IntegerField(primary_key=True)
	department_name = models.CharField(max_length=50,null=False,blank=False)

	def __unicode__(self):
		return str(self.department_name)+ '\n'

class Students(models.Model):
	admission_no = models.IntegerField(primary_key=True)
	student_name = models.CharField(max_length=150,null=False,blank=False)
	department_id = models.ManyToManyField(Department)

	def __unicode__(self):
		return str(self.admission_no)+' '+str(self.student_name)\
		+' '+ str(self.department_id)+'\n'

