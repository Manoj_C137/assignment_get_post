# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Department',
            fields=[
                ('department_id', models.IntegerField(serialize=False, primary_key=True)),
                ('department_name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Students',
            fields=[
                ('admission_no', models.IntegerField(serialize=False, primary_key=True)),
                ('student_name', models.CharField(max_length=120)),
                ('department_id', models.ForeignKey(to='SchoolApp.Department')),
            ],
        ),
    ]
