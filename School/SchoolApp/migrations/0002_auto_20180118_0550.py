# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('SchoolApp', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='department',
            name='department_name',
            field=models.CharField(max_length=50),
        ),
        migrations.RemoveField(
            model_name='students',
            name='department_id',
        ),
        migrations.AddField(
            model_name='students',
            name='department_id',
            field=models.ManyToManyField(to='SchoolApp.Department'),
        ),
        migrations.AlterField(
            model_name='students',
            name='student_name',
            field=models.CharField(max_length=150),
        ),
    ]
