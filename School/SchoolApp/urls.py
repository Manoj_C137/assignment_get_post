from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt
from SchoolApp.views import ViewStudent, ViewDepartment


urlpatterns = [
    url(r'^student/', csrf_exempt(ViewStudent.as_view())),
    url(r'^department/',csrf_exempt(ViewDepartment.as_view())),    
]