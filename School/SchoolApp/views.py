from django.shortcuts import render
from django.views.generic import View
from django.http import HttpResponse, QueryDict
from django.core.exceptions import ObjectDoesNotExist
from .models import Students,Department #import models of first app
from .exceptions import ForeignKeyError, StudentNotExist
from .utils import _validate_name, _validate_number

class ViewStudent(View):
	def post(self,request):
		_post_data = QueryDict(request.body) #post request data will be stored in _post_data #uuid

		_admission_no = _post_data.get('Admission_No')
		_validate_number(_admission_no) #checking student's admission number is valid or not
		_student_name = _post_data.get('Name')
		_student_name = " ".join(_student_name.split()) #removing extra spaces
		_validate_name(_student_name) #checking student's name is valid or not
		_temp_id = _post_data.get('Dept_ID')

		try:
			_dept_id = Department.objects.get(department_id=_temp_id ) #checking student's Department_id is present in Deparment
		except ObjectDoesNotExist:
			raise ForeignKeyError("There is no department with this Department ID")

		_student = Students(admission_no=_admission_no,student_name=_student_name)
		_student.save()
		_student.department_id.add(_dept_id)
		return HttpResponse("NEW DATA ADDEDD SUCCESSFULLY!")

	def get(self,request):
		_get_data = request.GET
		_admission_no = _get_data.get('Admission_No') #DB queries
		return HttpResponse(Students.objects.get(admission_no=_admission_no).department_id.all(),content_type='application/json')

	def put(self,request):
		_put_data = QueryDict(request.body)

		_admission_no = _put_data.get('Admission_No')
		_validate_number(_admission_no) #checking admission number is valid or not
		_student_name = _put_data.get('Name')
		_student_name = " ".join(_student_name.split()) #removing extra spaces
		_validate_name(_student_name) #checking student's name is valid or not
		_temp_id = _put_data.get('Dept_ID')

		try:
			_dept_id = Department.objects.get(department_id=_temp_id) #checking Department_id is present in Deparment
		except ObjectDoesNotExist:
			raise ForeignKeyError("There is no department with this Department ID")

		try:
			_student = Students.objects.get(admission_no=_admission_no) #checking Student is present
		except ObjectDoesNotExist:
			raise StudentNotExist("There is no Student with this Admission No")

		_student.student_name = _student_name
		_student.save()
		_student.department_id.add(_dept_id)

		return HttpResponse("DATA IS UPDATED SUCCESSFULLY")

class ViewDepartment(View):
	pass
	def post(self,request):
		_post_data = QueryDict(request.body) #post request data will be stored in _post_data

		_dept_id = _post_data.get('Dept_ID')
		_validate_number(_dept_id) #checking id is valid or not
		_dept_name = _post_data.get('Dept_Name')
		_validate_name(_dept_name) #checking department name is valid or not
		_department = Department(department_id=_dept_id,department_name=_dept_name)
		_department.save()

		return HttpResponse("New department ADDED")

	def get(self,request):
		_get_data = request.GET
		#Department.objects.delete()
		_departments = Department.objects.all() #to know all the departments, url: /school/department/
		return HttpResponse(_departments, content_type='application/json')