from django.conf.urls import include,url
from django.contrib import admin

urlpatterns = [
    url(r'^school/', include('SchoolApp.urls')),
    url(r'^admin/', include(admin.site.urls)),
]
