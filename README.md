"GET and POST requests using POSTMAN"
Project name is 'School' and app name 'SchoolApp'.

Has two models 'Students' with attributes admission_no, student_name, department_id and 'Department' with department_id, department_name, department_id as foreign key.
POSTMAN For Students: URL = /school/student/ 
			 keys (value) = Admission_No(IntegerField)
							Name (CharField)
							Dept_ID (IntegerField)
			
For Department: URL = /school/department/
	   keys (value) = Dept_ID (IntegerField)
					  Dept_Name (CharField)